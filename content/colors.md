---
title: "Color Palette"
draft: false
description: "Official color palette for the Ventura Villas"
type: "single"
---

All paints are Sherwin Williams.

| **DOORS - Semi Gloss** |         |
| ---                    | ---     |
| Cyberspace-Dark Blue   | SW 7076 |
| Slate Tile-Light Blue  | SW 7624 |
| City Loft-White        | SW 7631 |
| Antique Red-Red        | SW 7587 |

| **BUILDING - Satin** |       |         |
| ---                  | ---   | ---     |
| Skyline Steel        | Light | SW 1015 |
| Dovetail             | Dark  | SW 7018 |

| **FENCE - Gloss** |         |
| ---               | ---     |
| Rockweed          | SW 2735 |

| **Trim - Semi Gloss** |         |
| ---                   | ---     |
| City Loft-White       | SW 7631 |
