---
title: "Happy Hour at the Pool"
date: 2024-07-21 16:00:00
end_date: 2024-07-21 19:00:00
location: At the Ventura Villas Pool and Clubhouse
draft: false
---

You're invited! Please join us for Happy Hour at the pool!

Hot dogs, cookies and chips to be provided. BYOB.
Bring an appetizer to pass around if you'd like.
Bring the family and the kids and enjoy the pool!

### We hope to see you there!

