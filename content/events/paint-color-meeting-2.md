---
title: "Pre-Annual Meeting Event!"
date: 2022-03-19 11:00:00
end_date: 2022-03-19 14:00:00
location: Pool parking lot
draft: false
---

Checkout *NEW* paint color options!

Meet board members, ask questions about the upcoming annual meeting!

**LOWCOUNTRY Barbecue "Will Cook" will be there!**

- Pulled pork plate with 2 sides $16
- Pulled pork sandwich with 1 side $12
- Chicken quarter with 1 side $10
- Banana pudding $5

Please call or text Karen to RSVP so that food does not run out.

**We hope to see you there!**
